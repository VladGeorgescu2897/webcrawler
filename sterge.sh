#!/bin/bash

clear_docker () {
  docker rm $(docker ps -a -q)

  docker image rm webcrawler_apigateway
  docker image rm webcrawler_business-logic-service
  docker image rm webcrawler_crawler
  docker image rm webcrawler_auth-service
  docker image rm webcrawler_io-service

  docker network rm webcrawler_auth-db-network
  docker network rm webcrawler_business-logic-io-network
  docker network rm webcrawler_crawler-io-network
  docker network rm webcrawler_db-adminer-network
  docker network rm webcrawler_gateway-auth-network
  docker network rm webcrawler_gateway-business-logic-network
  docker network rm webcrawler_io-db-network

   docker volume rm webcrawler_db-persistent-volume
}

clear_docker
