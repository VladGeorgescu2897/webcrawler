const Router = require('express').Router();

const {
    ServerError
} = require('./errors');

const {
    makeRequest
} = require('./services.js');


Router.get('/news', async (req, res) => {
    console.info(`BL CONTROLLER: Receive from Gateway`);
    const news = await makeRequest();
    res.json(news);
});


module.exports = Router;
