const {
    sendRequest
} = require('./http-client');


const makeRequest = async () => {
    console.info(`BL SERVICES: receive from controller and forward to IO`);
    const makeRequest = {
            url: `http://${process.env.IO_SERVICE_API_ROUTE}/news`,
            method: "GET",
    }
    const news = await sendRequest(makeRequest);

    return news;
};

module.exports = {
    makeRequest
}
