import crochet
crochet.setup()

from flask import Flask , render_template, jsonify, request, redirect, url_for
from scrapy import signals
from scrapy.crawler import CrawlerRunner
from scrapy.signalmanager import dispatcher
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import time
import requests
import os
import json

# Importing our Scraping Function from the amazon_scraping file

from tutorial.myspider import BlogSpider


# Creating Flask App Variable

app = Flask(__name__)

output_data = []
crawl_runner = CrawlerRunner()

# By Deafult Flask will come into this when we run the file
@app.route('/scrape-bbc')
def index():
    scrape_with_crochet(baseURL='https://www.bbc.com/') # Passing that URL to our Scraping Function
    sid_obj = SentimentIntensityAnalyzer()
    time.sleep(1) # Pause the function while the scrapy spider is running

    news_to_send = []
    for news in output_data[0]["items"]:
        legit_title = True
        legit_description = True
        title = news["title"]
        description = news["summary"]
        if title != "None" and description != "None":
            title_scores = sid_obj.polarity_scores(title)
            description_scores = sid_obj.polarity_scores(description)
            if title_scores['neg'] >= 0.35 or (
                title_scores['neg'] < 0.35 and
                title_scores['neg'] >= 0.2 and
                title_scores['compound'] < -0.25):
                    legit_title = False
            if description_scores['neg'] >= 0.35 or (
                description_scores['neg'] < 0.35 and
                description_scores['neg'] >= 0.2 and
                description_scores['compound'] < -0.25):
                    legit_description = False
            if legit_title and legit_description:
                news_to_send.append({
                    'imageSrc' : news["imageSrc"],
                    'title' : news["title"],
                    'summary' : news["summary"],
                    'link' : news["link"],
                    'tag' : news["tag"],
                    'tagLink' : news["tagLink"],
                })
            else:
                print("T: {}: {} : {} : {} : {}".format(title, title_scores['neg'], title_scores['neu'], title_scores['pos'], title_scores["compound"]))
                print("D: {}: {} : {} : {} : {}".format(description, description_scores['neg'], description_scores['neu'], description_scores['pos'], description_scores["compound"]))
        elif description == "None" and title != "None":
            title_scores = sid_obj.polarity_scores(title)
            if title_scores['neg'] >= 0.35 or (
                title_scores['neg'] < 0.35 and
                title_scores['neg'] >= 0.2 and
                title_scores['compound'] < -0.25):
                    legit_title = False
            if legit_title and legit_description:
                news_to_send.append({
                    'imageSrc' : news["imageSrc"],
                    'title' : news["title"],
                    'summary' : news["summary"],
                    'link' : news["link"],
                    'tag' : news["tag"],
                    'tagLink' : news["tagLink"],
                })
            else:
                print("Title: {}: {} : {} : {} : {}".format(title, title_scores['neg'], title_scores['neu'], title_scores['pos'], title_scores["compound"]))
    return jsonify(news_to_send)


@crochet.run_in_reactor
def scrape_with_crochet(baseURL):
    # This will connect to the dispatcher that will kind of loop the code between these two functions.
    dispatcher.connect(_crawler_result, signal=signals.item_scraped)

    # This will connect to the ReviewspiderSpider function in our scrapy file and after each yield will pass to the crawler_result function.
    eventual = crawl_runner.crawl(BlogSpider, category = baseURL)
    return eventual

#This will append the data to the output data list.
def _crawler_result(item, response, spider):
    output_data.append(dict(item))


if __name__== "__main__":
    app.run(debug=True)
