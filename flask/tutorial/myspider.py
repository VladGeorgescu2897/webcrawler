import scrapy
import psycopg2

class BlogSpider(scrapy.Spider):
    name = 'blogspider'
    myBaseUrl = ''
    start_urls = []
    def __init__(self, category='', **kwargs): # The category variable will have the input URL.
        self.myBaseUrl = category
        self.start_urls.append(self.myBaseUrl)
        super().__init__(**kwargs)
    custom_settings = {'FEED_URI': 'tutorial/outputfile.json', 'CLOSESPIDER_TIMEOUT' : 15} # This will tell scrapy to store the scraped data to outputfile.json and for how long the spider should run.

    def parse(self, response):
        print("url1: ", self.start_urls)
        count = 1
        items = []
        imageSrcAll = []
        titleAll = []
        summaryAll = []
        linkAll = []
        tagAll = []
        tagLinkAll = []
        if self.myBaseUrl == 'https://www.bbc.com/':
            print("BBC")
            for item in response.css('li.media-list__item'):
                imageSrc = item.css('img').xpath('@src').get()
                summary = item.css('.media__summary').css('::text').get()
                title = item.css('.media__title').css('a').css('::text').get()
                link = item.css('.media__title').css('a').xpath('@href').get()
                tag = item.css('.media__tag').css('::text').get()
                tagLink = item.css('.media__tag').xpath('@href').get()
                if imageSrc != None:
                    # print(count, " : Image source : " + imageSrc)
                    imageSrcAll.append(imageSrc)
                else:
                    # print(count, " : Image source : None")
                    imageSrcAll.append("None")
                if title != None:
                    # print(count, " : Title : " + title.strip())
                    titleAll.append(title.strip())
                else:
                    # print(count, " : Title : None")
                    titleAll.append("None")
                if summary != None:
                    print(count, " : Summary : " + summary.strip())
                    summaryAll.append(summary.strip())
                else:
                    # print(count, " : Summary : None")
                    summaryAll.append("None")
                if link != None:
                    # print(count, " : Link : " + link)
                    linkAll.append(link)
                else:
                    # print(count, " : Link : None")
                    linkAll.append("None")
                if tag != None:
                    # print(count, " : Tag : " + tag)
                    tagAll.append(tag)
                else:
                    # print(count, " : Tag : None")
                    tagAll.append("None")
                if tagLink != None:
                    # print(count, " : Tag link : " + tagLink)
                    tagLinkAll.append(tagLink)
                else:
                    # print(count, " : Tag link : None")
                    tagLinkAll.append("None")
                # print("---------------------------------------------------------------------------------------------")

                items.append({
                'imageSrc' : imageSrcAll[count - 1],
                'title' : titleAll[count - 1],
                'summary' : summaryAll[count - 1],
                'link' : linkAll[count - 1],
                'tag' : tagAll[count - 1],
                'tagLink' : tagAll[count - 1],
                })
                count += 1
        if self.myBaseUrl == 'https://www.skysports.com/':
            print("SKYSPORTS")
            for item in response.css('div.sdc-site-tiles__item'):
                title = item.css('span.sdc-site-tile__headline-text').css('::text').get()
                link = item.css('a.sdc-site-tile__headline-link').xpath('@href').get()
                tag = item.css('a.sdc-site-tile__tag-link').css('::text').get()
                tagLink = item.css('a.sdc-site-tile__tag-link').xpath('@href').get()
                print(" Title: ",  title)
                print(" Link: ",  link)
                print(" Tag: ",  tag)
                print(" Tag link: ",  tagLink)
                print("---------------------------------------------------------------------------------------------")
                count += 1
        if self.myBaseUrl == 'https://www.nature.com/news':
            print("NATURE")
            for item in response.css('div.c-card__container'):
                title = item.css('h3').css('::text').get()
                summary = item.css('div.c-card__standfirst').css('::text').get()
                link = item.css('a').xpath('@href').get()
                print("Title: ",  title)
                print("Link: ",  link)
                if summary != None:
                    print("Summary: ", summary.strip())
                    # summaryAll.append(summary.strip())
                else:
                    print("Summary: None")
                    # summaryAll.append("None")
                print("---------------------------------------------------------------------------------------------")
                count += 1
        # print("Items: ", len(items))
        self.start_urls.pop(0)
        print("url1: ", self.start_urls)
        # self.start_urls = []
        yield {'items' : items}
