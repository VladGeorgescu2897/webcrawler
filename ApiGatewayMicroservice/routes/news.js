const Router = require("express").Router();

const { sendRequest } = require("../http-client");

Router.get("/", async (req, res) => {
  console.info(`Forwarding request to make request for NEWS in GATEWAY...`);


  const makeRequest = {
    url: `http://${process.env.BUSINESS_LOGIC_SERVICE_API_ROUTE}/news`,
    method: "GET",
  };

  const response = await sendRequest(makeRequest);

  res.json(response);
});

module.exports = Router;
