const Router = require('express').Router();
const AuthRoute = require('./auth.js');
const NewsRoute = require('./news.js');
Router.use('/auth', AuthRoute);
Router.use('/news', NewsRoute);

module.exports = Router;
