const Router = require("express").Router();

const { sendRequest } = require("../http-client");

Router.post("/login", async (req, res) => {
    console.info(`Forwarding request for login ...`);

    const { username, password} = req.body;

    const getResponseRequest = {
        url: `http://${process.env.AUTH_SERVICE_API_ROUTE}/login`,
        method: "GET",
        data: {
            username,
            password
        },
    };

    const getUserRequest = {
        url: `http://${process.env.AUTH_SERVICE_API_ROUTE}/client`,
        method: "GET",
        data: {
            username
        },
    };

    const string = await sendRequest(getResponseRequest);
    const user = await sendRequest(getUserRequest);

    const response = {
        token: string,
        userId: user[0].client_id,
        userRole: user[0].role
    }

    res.status(200).json(response);
});

module.exports = Router;
