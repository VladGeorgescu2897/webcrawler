const axios = require('axios').default;

const sendRequest = async (options) => {
    try {
        const { data } = await axios(options);
        console.log("sendRequest " + data)
        return data;
    } catch (error) {
        throw error;
    }
}

module.exports = {
    sendRequest
}
