CREATE TABLE IF NOT EXISTS clients (
    client_id serial PRIMARY KEY,
    first_name varchar NOT NULL,
    last_name varchar NOT NULL,
    username varchar NOT NULL,
    password varchar NOT NULL,
    email varchar NOT NULL,
    role varchar NOT NULL
);

CREATE TABLE IF NOT EXISTS news (
    news_id serial PRIMARY KEY,
    first_name varchar NOT NULL,
    last_name varchar NOT NULL,
    username varchar NOT NULL,
    password varchar NOT NULL,
    email varchar NOT NULL,
    role varchar NOT NULL
);

INSERT INTO clients (first_name, last_name, username, password, email, role)
  VALUES ('Admin', 'Adminus', 'admin', 'password', 'admin@gmail.com', 'admin');
