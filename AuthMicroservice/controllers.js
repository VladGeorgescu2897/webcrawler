const Router = require('express').Router();

const {
    ServerError
} = require('./errors');

const {
    addClient,
    authenticate,
    verifyAndDecodeData,
    getClientByUsername
} = require('./services.js');

Router.get('/login', async (req, res) => {

    const {
        username,
        password
    } = req.body;

    const token = await authenticate(username, password)

    res.json(token);
});

Router.get('/client', async (req, res) => {

    const {
        username
    } = req.body;

    const token = await getClientByUsername(username)

    res.json(token);
});

Router.post('/register', async (req, res) => {
  console.log("microservice: " + req.body.name)
    const {
      first_name,
      last_name,
      username,
      password,
      email,
      role,
      birthday,
      id_number,
      address,
      phone,
      occupation,
      domain,
      employer
    } = req.body;

    const id = await addClient(first_name, last_name, username, password, email, role, birthday, id_number, address, phone, occupation, domain, employer);

    res.json(id);
});

Router.get('/verify', async (req, res) => {

    const token = req.headers.authorization;
    const { rolesToCheck } = req.body;

    const hasPermission = await verifyAndDecodeData(token, rolesToCheck);

    res.json(hasPermission);
});

module.exports = Router;
