const {
    sendRequest
} = require('./http-client');

const jwt = require('jsonwebtoken');

const options = {
    issuer: process.env.JWT_ISSUER,
    subject: process.env.JWT_SUBJECT,
    audience: process.env.JWT_AUDIENCE
};

const {
    query
} = require('./data');

const generateToken = async (payload) => {
    try {
        const token = await jwt.sign(payload, process.env.JWT_SECRET_KEY, options);
        return token;
    } catch (err) {
        console.trace(err);
    }
};

const verifyAndDecodeData = async (token, rolesToCheck) => {
    try {
        const decoded = await jwt.verify(token, process.env.JWT_SECRET_KEY, options);
        return rolesToCheck.includes(decoded.clientRole);
    } catch (err) {
        console.trace(err);
    }
};

const addClient = async (first_name, last_name, username, password, email, role, birthday, id_number,
  address, phone, occupation, domain, employer) => {
    console.info(`Sending request to db to add client with username ${username} and
      email ${email} ...`);

    const clients = await query("INSERT INTO clients (first_name, last_name, username, \
      password, email, role, birthday, id_number, address, phone, occupation, domain, \
      employer) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13) RETURNING \
      client_id", [first_name, last_name, username, password, email, role, birthday,
        id_number, address, phone, occupation, domain, employer]);

    return clients[0].client_id;
};

const authenticate = async (username, password) => {
    console.info(`Sending request to db to login ...`);

    const client = await query("SELECT * FROM clients WHERE username = '" + username
      + "'");
    let token = "";
    if (password === client[0].password) {
        token = await generateToken({
            clientId: client[0].client_id,
            clientRole: client[0].role
        })
    }

    return token;
};

const getClientByUsername = async (username) => {
    console.info(`Sending request to db to get client with username ${username} ...`);

    const client = await query("SELECT * FROM clients WHERE username = '" + username
      + "'");

    return client;
};

module.exports = {
    addClient,
    authenticate,
    verifyAndDecodeData,
    getClientByUsername
}
