const {
    query
} = require('../data');
const {
    sendRequest
} = require('../http-client');

const getNews = async () => {
    console.info(`IO SERVICES: receive from controller and forward crawler`);

    const makeRequest = {
        url: `http://${process.env.CRAWLER_SERVICE_API_ROUTE}:5000/scrape-bbc`,
        method: "GET",
    }

    const news = await sendRequest(makeRequest);
    return news;
};


module.exports = {
    getNews,
}
