const Router = require('express').Router();

const {
    getNews
} = require('./services.js');

Router.get('/', async (req, res) => {
    console.info(`IO CONTROLLERS: receive request from BL`);
    const clients = await getNews();

    res.json(clients);
});

module.exports = Router;
