const Router = require('express').Router();

const NewsController = require('./news/controllers.js');

Router.use('/news', NewsController);

module.exports = Router;
